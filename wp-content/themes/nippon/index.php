<?php get_header(); ?>
   <div class="top_content clearfix">
       <h2 class="title"><img src="<?php bloginfo('template_directory'); ?>/img/top/top_tit01.png" alt="ようこそ、メープルツリーガーデンへ！" /></h2>
       <div class="top_sec01 clearfix">
           <div class="top_left">
               <div class="left01">
                   <img src="<?php bloginfo('template_directory'); ?>/img/top/top_img01.png" alt="top_img01" class="img01" />
               </div><!-- left01 -->
               <div class="right01">
                  <p class="top_txt01 pad_B30">2016年12月、千葉県佐倉市にオープンしました。<br />
                  ドッグランを併設したカフェです。（ドッグランのみのご利用はできません。）</p>
                  <p class="top_txt01 pad_B30">カフェは、いわゆるワンちゃん連れだけが楽しみ集う　”ドッグカフェ”　ではありません。<br />
                  ワンちゃんが好きな方、見たい方、北欧の気分を感じながら大人空間で過ごしたい方。<br />
                  大人が居心地良ければ、ワンちゃんも居心地が良いはず！そんな空間を追求しました。<br />
                  （誠に勝手ながら、小学生以下のお子様のご入店はご遠慮させていただきます。）</p>
                  <p class="top_txt01 pad_B30">貸切や天候不良によりご利用できない日もございますので、<br />必ず営業カレンダーをご確認の上ご来店いただけますようお願い申し上げます。</p>
                  <p class="top_txt01 pad_B20">当施設のドッグランは小型犬専用とさせていただきます。
                  （ドッグラン利用登録の際は市区町村から発行される狂犬病予防注射済みの鑑札と病院で発行されるワクチン接種済の証明書（原本）をご持参ください。）</p>
               </div><!-- right01 -->
           </div><!-- top_left -->
           <div class="top_right">
              <img src="<?php bloginfo('template_directory'); ?>/img/top/top_img02.png" alt="top_img02" class="img02" />
           </div><!-- top_right -->

           <div class="top_btn01 clearfix">
               <a href="attention.html#business" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/img/top/top_btn01.png" alt="top_btn01" /></a>
           </div>
       </div><!-- top_sec01 -->

       <div class="top_sec02 clearfix">
           <div class="sec02In clearfix">
              <div class="sec02_left">
                  <h3 class="tit01 sp_tit_wid"><img src="<?php bloginfo('template_directory'); ?>/img/top/top_tit02.png" alt="top_tit02" /></h3>
                  <ul class="top_new clearfix">
                   <?php
                    $loop = new WP_Query(array("post_type" => "news","posts_per_page" => 5,"paged"=>$paged, 'order' => ASC));
                    if ( $loop->have_posts() ) : while($loop->have_posts()): $loop->the_post();
                    ?>
                     <li class="clearfix">
                        <span class="list_date"><?php the_time('Y.m.d'); ?></span>
                        <span class="list_txt"><a href="<?php echo get_post_type_archive_link( 'news' ); ?>"><?php the_title(); ?></a></span>
                     </li>
                     <?php endwhile; endif; ?>
                    
                  </ul>
              </div><!-- sec02_left -->
              <div class="sec02_right clearfix">
                 <div class="sec02_l">
                    <h3 class="tit01 sp_tit_wid01"><img src="<?php bloginfo('template_directory'); ?>/img/top/top_tit03.png" alt="top_tit03" /></h3>
                    <p>カフェ店内は1970年代に製造されたランプシェードやテーブルなど北欧やイギリスからの文化と現代が心地よく融合しています。<br>
                    挽きたて豆の珈琲、香り高い紅茶、本格オーブンで焼いた自家製パンを使用したメニューをご用意しております。</p>
                 </div>
                 <div class="sec02_r">
                     <img src="<?php bloginfo('template_directory'); ?>/img/top/top_img03.png" alt="top_img03" />
                 </div>
              </div><!-- sec02_right -->
           </div><!-- sec02In -->
           <div class="sec02In sec02Pad clearfix">
              <div class="sec02_left clearfix">
                  <div class="sec02_l">
                    <h3 class="tit01 sp_tit_wid03"><img src="<?php bloginfo('template_directory'); ?>/img/top/top_tit04.png" alt="top_tit03" /></h3>
                    <p>ドッグランのご利用につきましては、ご利用時に飼い主様とワンちゃんの利用登録を行っていただくか、ビジター利用のいずれかをご選択いただきます。ご利用前に必ずカフェで利用開始を申し出てください。</p>
                    <div class="top_btn01 clearfix">
                      <a href="<?php bloginfo('template_directory'); ?>/pdf/usage.pdf"><img src="<?php bloginfo('template_directory'); ?>/img/top/top_btn02.png" alt="top_btn02" /></a>
                    </div>
                 </div>
                 <div class="sec02_r">
                     <img src="<?php bloginfo('template_directory'); ?>/img/top/top_img04.png" alt="top_img04" />
                 </div>
              </div><!-- sec02_left -->
              <div class="sec02_right clearfix">
                 <div class="sec02_l">
                    <h3 class="tit01"><img src="<?php bloginfo('template_directory'); ?>/img/top/top_tit05.png" alt="top_tit05" /></h3>
                    <p>ご登録の前に必ずご利用規約をお読み頂き、同意された方のみ必要事項をご記入の上、お店にお持ち下さい。<br>
                    また店内でご記入頂く事も可能です。</p>
                    <div class="top_btn01 clearfix">
                      <a href="<?php bloginfo('template_directory'); ?>/pdf/usage.pdf"><img src="<?php bloginfo('template_directory'); ?>/img/top/top_btn03.png" alt="top_btn03" /></a>
                    </div>
                 </div>
                 <div class="sec02_r">
                     <img src="<?php bloginfo('template_directory'); ?>/img/top/top_img05.png" alt="top_img05" />
                 </div>
              </div> <!-- sec02_right-->
           </div><!-- sec02In -->
           <div class="sec02In sec02Pad clearfix">
              <div class="sec02_left clearfix">
                  <div class="sec02_l">
                    <h3 class="tit01 sp_tit_wid02"><img src="<?php bloginfo('template_directory'); ?>/img/top/top_tit06.png" alt="top_tit06" /></h3>
                    <p>MTRGドッグランの登録料や利用料金、貸切のご案内です。</p>
                    <div class="top_btn01 clearfix">
                      <a href="<?php bloginfo('template_directory'); ?>/pdf/usage.pdf"><img src="<?php bloginfo('template_directory'); ?>/img/top/top_btn04.png" alt="top_btn04" /></a>
                    </div>
                 </div>
                 <div class="sec02_r">
                     <img src="<?php bloginfo('template_directory'); ?>/img/top/top_img06.png" alt="top_img06" />
                 </div>
              </div><!-- sec02_left -->
              <div class="sec02_right clearfix">
                 <div class="sec02_l">
                    <h3 class="tit01 sp_tit_wid02"><img src="<?php bloginfo('template_directory'); ?>/img/top/top_tit07.png" alt="top_tit07" /></h3>
                    <p>貸切のご予約や、撮影利用など、<br>
                    またアートスペースとしてのご利用も承っております。<br>
                    その他、ご質問などはこちらからお願い致します。</p>
                    <div class="top_btn01 clearfix">
                      <a href="<?php bloginfo('template_directory'); ?>/pdf/usage.pdf"><img src="<?php bloginfo('template_directory'); ?>/img/top/top_btn05.png" alt="top_btn05" /></a>
                    </div>
                 </div>
                 <div class="sec02_r">
                    <img src="<?php bloginfo('template_directory'); ?>/img/top/top_img07.png" alt="top_img07" />
                 </div>
              </div> <!-- sec02_right-->
           </div><!-- sec02In -->
       </div><!-- top_sec02 -->
    </div><!-- top_content -->
</div><!-- wrapper --> 
<?php get_footer(); ?>