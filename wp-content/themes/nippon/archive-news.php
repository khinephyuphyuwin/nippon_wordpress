<?php get_header(); ?>
	 <div class="content">
	 	<div class="news">
      <div class=" cmn_sec">

        <div class="play">
        <?php
            $loop = new WP_Query(array("post_type" => "news","posts_per_page" => 3,"paged"=>$paged, 'order' => ASC));
            if ( $loop->have_posts() ) : while($loop->have_posts()): $loop->the_post();
            ?>
          <h2><a href="<?php the_permalink() ?>"><?php the_title(); ?><span class="text"><?php the_time('Y.m.d'); ?></span></a></h2>
          <p><?php the_content(); ?></p>
          <div class="play_img clearfix">
            <?php $img01 = get_field('img01',$post->ID);
             if( !empty($img01) ): ?>
             <img src="<?php echo $img01['url']; ?>" alt="<?php echo $img01['alt']; ?>" />
           <?php endif;?>
            <?php $img02 = get_field('img02',$post->ID);
             if( !empty($img02) ): ?>
             <img src="<?php echo $img02['url']; ?>" alt="<?php echo $img02['alt']; ?>" />
           <?php endif;?>
          </div>
          <?php endwhile; endif; ?>
        </div><!-- .play -->
        <?php wp_pagenavi(); ?>

      </div><!-- cmn_sec -->

    </div><!-- .news -->
	 </div>
	  </div><!-- wrapper !-->
<?php get_footer(); ?>