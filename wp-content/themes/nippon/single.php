<?php get_header(); ?>


   <div class="content"> 
    <div class=" cmn_sec">
      <div class="singlesec play">
       <h2><?php the_title(); ?> <span class="text"><?php the_time('Y.m.d'); ?></span></h2>
        <p class="singlecontent">
          <?php 
          while( have_posts() ): the_post();
            the_content('');
          endwhile;
        ?>
        </p>
       <div class="play_img clearfix">
          <?php $img01 = get_field('img01',$post->ID);
               if( !empty($img01) ): ?>
               <img src="<?php echo $img01['url']; ?>" alt="<?php echo $img01['alt']; ?>" />
          <?php endif;?>
          <?php $img02 = get_field('img02',$post->ID);
               if( !empty($img02) ): ?>
               <img src="<?php echo $img02['url']; ?>" alt="<?php echo $img02['alt']; ?>" />
          <?php endif;?>
        </div>
      </div>
  </div><!-- content -->
  
    </div><!-- wrapper !-->
<?php get_footer(); ?>