<!DOCTYPE html>
<?php global $data; ?>
<html>
  <head <?php language_attributes(); ?> >
    <title><?php bloginfo('name'); ?><?php wp_title(); ?></title>
    <meta charset="<?php bloginfo('charset') ?>" />
    <meta http-equiv="content-script-type" content="text/javascript">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0,  user-scalable=1">
    <meta name="description" content="ナモーカムカルチャースタジオは、世界で一番大きな電磁波シールドルームを構え、大阪市生野区で、バレエ、ジャズ、タップ、コンテンポラリー、モデルウォーキング、空手、ピアノ、サックス、ドラム、ボイストレーニング、ピラテスを指導しているカルチャースクールです。素晴らしい芸術を通じて、豊かな感受性と深い道徳心を持った人間関係形成を育むことにあります。">
    <meta name="keywords" content="ナモーカムカルチャースクール,NAMOHCAM,大阪,生野区,バレエ,空手,コンテンポラリー,ボイストレーニング,音楽教室,舎利寺">
    <link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/common/img/header/favicon.png" />
    <link href="https://fonts.googleapis.com/css?family=Raleway:400,600" rel="stylesheet">
    <script type="text/javascript">
    if ((navigator.userAgent.indexOf('iPhone') > 0 && navigator.userAgent.indexOf('iPad') == -1) || navigator.userAgent.indexOf('iPod') > 0 || navigator.userAgent.indexOf('Android') > 0) {
      document.write('<meta name="viewport" content="width=device-width">');
    } else {
      document.write('<meta name="viewport" content="width=1024">');
    }
  </script>
    <?php wp_head(); ?>
  </head>
  <body class="top">
<div id="wrapper">
<?php if(is_home())
    {?>

    <div class="header clearfix">  
        <div class="top_bnr clearfix">           
            <ul class="bxslider">
                <li><img src="<?php bloginfo('template_directory'); ?>/img/top/slide01.jpg" alt="slide01"></li>
                <li><img src="<?php bloginfo('template_directory'); ?>/img/top/slide02.jpg" alt="slide02"></li>
                <li><img src="<?php bloginfo('template_directory'); ?>/img/top/slide03.jpg" alt="slide03"></li>
            </ul>
         </div> <!-- top_bnr -->
         <h1 class="h_text">千葉県 佐倉市 ドックラン＆カフェ　メープルツリーガーデン</h1> 
         <div class="logo">
            <img src="<?php bloginfo('template_directory'); ?>/common/img/header/logo.png" alt="logo" />
         </div>        
     </div> <!-- header -->
    <div class="nav pc clearfix">
      <ul class="gnav clearfix">
        <li><a href="<?php echo site_url(); ?>" class="gnav01">TOP<span>トップ</span></a></li>
        <li><a href="<?php echo get_permalink( get_page_by_path( 'attention' ) ) ?>" class="gnav02">ATTENTION<span>ご利用案内</span></a></li>
        <li><a href="<?php echo get_permalink( get_page_by_path( 'dog_run' ) ) ?>" class="gnav03">DOG RUN<span>ドッグラン</span></a></li>
        <li><a href="<?php echo get_permalink( get_page_by_path( 'price' ) ) ?>" class="gnav04">PRICE<span>ご利用料金</span></a></li>
        <li><a href="<?php echo get_permalink( get_page_by_path( 'access' ) ) ?>" class="gnav05">ACCESS<span>アクセス</span></a></li>
        <li><a href="<?php echo get_permalink( get_page_by_path( 'about' ) ) ?>" class="gnav06">ABOUT US<span>MTRGについて</span></a></li>
        <li><a href="<?php echo get_permalink( get_page_by_path( 'contact' ) ) ?>" class="gnav07">CONTACT<span>お問い合わせ</span></a></li>
      </ul> <!-- gnav-->
    </div><!-- nav-->
    <nav id="mxNav" class="sp">
      <div id="mxWrap"></div>
      <div id="hLine">
        <h1>TOP<span>トップ</span></h1>
        <div id="mxIcn">
          <div class="icnEle"></div>
          <div class="icnEle"></div>
          <div class="icnEle"></div>
        </div>
      </div>

      <ul id="mxNavLi">
        <li><a href="<?php echo site_url(); ?>" class="gnav01">TOP<span>トップ</span></a></li>
        <li><a href="<?php echo get_permalink( get_page_by_path( 'attention' ) ) ?>" class="gnav02">ATTENTION<span>ご利用案内</span></a></li>
        <li><a href="<?php echo get_permalink( get_page_by_path( 'dog_run' ) ) ?>" class="gnav03">DOG RUN<span>ドッグラン</span></a></li>
        <li><a href="<?php echo get_permalink( get_page_by_path( 'price' ) ) ?>" class="gnav04">PRICE<span>ご利用料金</span></a></li>
        <li><a href="<?php echo get_permalink( get_page_by_path( 'access' ) ) ?>" class="gnav05">ACCESS<span>アクセス</span></a></li>
        <li><a href="<?php echo get_permalink( get_page_by_path( 'about' ) ) ?>" class="gnav06">ABOUT US<span>MTRGについて</span></a></li>
        <li><a href="<?php echo get_permalink( get_page_by_path( 'contact' ) ) ?>" class="gnav07">CONTACT<span>お問い合わせ</span></a></li>
      </ul>
    </nav>
    <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/common/js/mxToggle.js"></script>
<?php }?>
<?php if(is_page()||is_category()||is_single()||is_archive())
    {?>
    <div class="header">  
      <div <?php if ( is_page('about')) 
       { echo ' class="about_bnr"';} 
        if ( is_page('access')) 
       { echo ' class="access_bnr"';} 
       if ( is_page('attention')) 
         { echo ' class="attention_bnr"';}
       if ( is_page('contact')) 
         { echo ' class="contact_bnr"';}
       if ( is_page('dog_run')) 
         { echo ' class="dog_run_bnr"';}
       if ( is_archive('news')) 
         { echo ' class="news_bnr"';}
       if ( is_single()) 
         { echo ' class="news_bnr"';}
       if ( is_page('price')) 
         { echo ' class="price_bnr"';}
        if ( is_page('privacypolicy')) 
         { echo ' class="privacy_bnr"';}
      ?>
      >
        <div class="content bnr_txt">
          <a href="<?php echo site_url(); ?>"><img src="<?php bloginfo('template_directory'); ?>/common/img/header/header_logo_img01.png" alt="header logo"></a>
        </div>
      </div>  
       <nav class="nav_sty02 pc">
        <div class="inner clearfix">
          <h2 class="pg_tit"><?php  echo get_the_post_thumbnail( $post->ID, 'page-thumb' ); ?></h2>
          <ul class="clearfix">
            <li><a href="<?php echo site_url(); ?>" class="gnav01">TOP<span>トップ</span></a></li>
            <li <?php if ( is_page('attention')) 
              { echo 'class="attention_nav"';}  
            ?>>
          <a href="<?php echo get_permalink( get_page_by_path( 'attention' ) ) ?>" <?php if ( is_page('attention')) { echo ' class="active_color"'; } ?> class="gnav02">ATTENTION<span>ご利用案内</span></a>
          </li>
            <li <?php if ( is_page('dog_run')) 
              { echo 'class="dog_run_nav"';}  
            ?>><a href="<?php echo get_permalink( get_page_by_path( 'dog_run' ) ) ?>" class="gnav03">DOG RUN<span>ドッグラン</span></a></li>
            <li <?php if ( is_page('price')) 
              { echo 'class="price_nav"';}  
            ?>><a href="<?php echo get_permalink( get_page_by_path( 'price' ) ) ?>" class="gnav04">PRICE<span>ご利用料金</span></a></li>
            <li <?php if ( is_page('access')) 
              { echo 'class="access_nav"';}  
            ?>><a href="<?php echo get_permalink( get_page_by_path( 'access' ) ) ?>" class="gnav05">ACCESS<span>アクセス</span></a></li>
            <li <?php if ( is_page('about')) 
              { echo 'class="about_nav"';}  
            ?>><a href="<?php echo get_permalink( get_page_by_path( 'about' ) ) ?>" class="gnav06">ABOUT US<span>MTRGについて</span></a></li>
            <li <?php if ( is_page('contact')) 
              { echo 'class="contact_nav"';}  
            ?>><a href="<?php echo get_permalink( get_page_by_path( 'contact' ) ) ?>" class="gnav07">CONTACT<span>お問い合わせ</span></a></li>
          </ul>
        </div>
      </nav>    
    </div> <!-- header -->

    <nav id="mxNav" class="sp">
        <div id="mxWrap"></div>
        <div id="hLine">
          <h1>
            <?php 
              if ( is_page('about')) 
              { ?>
                ABOUT US<span>MTRGについて</span> 
              <?php 
              }
              if ( is_page('access')) 
              { ?>
              ACCESS<span>アクセス</span> 
              <?php 
              }
              if ( is_page('attention')) 
              { ?>
              ATTENTION<span>ご利用案内</span> 
              <?php 
              }
              if ( is_page('contact')) 
              { ?>
              CONTACT<span>お問い合わせ</span> 
              <?php 
              }
              if ( is_page('dog_run')) 
              { ?>
              DOG RUN<span>ドッグラン</span> 
              <?php 
              }
              if ( is_archive('news')) 
              { ?>
              NEWS & INFORMATION<span>お知らせ</span> 
              <?php 
              }
               if ( is_single()) 
              { ?>
              NEWS & INFORMATION<span>お知らせ</span> 
              <?php 
              }
              if ( is_page('price')) 
              { ?>
              PRICE<span>ご利用料金</span> 
              <?php 
              }
              if ( is_page('privacypolicy')) 
              { ?>
              PRIVACYPOLICY<span>個人情報保護方針</span> 
              <?php 
              }
            ?>
          </h1>
          <div id="mxIcn">
            <div class="icnEle"></div>
            <div class="icnEle"></div>
            <div class="icnEle"></div>
          </div>
        </div>

        <ul id="mxNavLi">
          <li><a href="<?php echo site_url(); ?>" class="gnav01">TOP<span>トップ</span></a></li>
          <li><a href="<?php echo get_permalink( get_page_by_path( 'attention' ) ) ?>" class="gnav02">ATTENTION<span>ご利用案内</span></a></li>
          <li><a href="<?php echo get_permalink( get_page_by_path( 'dog_run' ) ) ?>" class="gnav03">DOG RUN<span>ドッグラン</span></a></li>
          <li><a href="<?php echo get_permalink( get_page_by_path( 'price' ) ) ?>" class="gnav04">PRICE<span>ご利用料金</span></a></li>
          <li><a href="<?php echo get_permalink( get_page_by_path( 'access' ) ) ?>" class="gnav05">ACCESS<span>アクセス</span></a></li>
          <li>
          <a href="<?php echo get_permalink( get_page_by_path( 'about' ) ) ?>" class="gnav06" >ABOUT US<span>MTRGについて</span></a></li>
          <li><a href="<?php echo get_permalink( get_page_by_path( 'contact' ) ) ?>" class="gnav07">CONTACT<span>お問い合わせ</span></a></li>
        </ul>
    </nav>
    <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/common/js/mxToggle.js"></script>
<?php }?>
    