<div class="sidebar">
        <div class="sidebarIn">
          <p class="sidelogo"><a href="<?php echo site_url(); ?>"><img src="<?php bloginfo('template_directory'); ?>/common/img/sidebar/sidelogo.png" alt="" /></a></p>
          <p class="sidetit"><img src="<?php bloginfo('template_directory'); ?>/common/img/sidebar/side_o01.png" alt="" /></p>
          <ul class="sideList">
            <li><a href="<?php echo get_permalink( get_page_by_path( 'dance' ) ) ?>#ballet" class="scroll01"><img src="<?php bloginfo('template_directory'); ?>/common/img/sidebar/side_o02.png" alt="" /></a></li>
            <li><a href="<?php echo get_permalink( get_page_by_path( 'dance' ) ) ?>#jazz" class="scroll01"><img src="<?php bloginfo('template_directory'); ?>/common/img/sidebar/side_o03.png" alt="" /></a></li>
            <li><a href="<?php echo get_permalink( get_page_by_path( 'dance' ) ) ?>#tap" class="scroll01"><img src="<?php bloginfo('template_directory'); ?>/common/img/sidebar/side_o04.png" alt="" /></a></li>
            <li><a href="<?php echo get_permalink( get_page_by_path( 'dance' ) ) ?>#contemporary" class="scroll01"><img src="<?php bloginfo('template_directory'); ?>/common/img/sidebar/side_o05.png" alt="" /></a></li>
            <li><a href="<?php echo get_permalink( get_page_by_path( 'pilates' ) ) ?>#modelwalking" class="scroll01"><img src="<?php bloginfo('template_directory'); ?>/common/img/sidebar/side_o06.png" alt="" /></a></li>
            <li><a href="<?php echo get_permalink( get_page_by_path( 'karate' ) ) ?>#karate" class="scroll01"><img src="<?php bloginfo('template_directory'); ?>/common/img/sidebar/side_o07.png" alt="" /></a></li>
            <li><a href="<?php echo get_permalink( get_page_by_path( 'music' ) ) ?>#piano" class="scroll01"><img src="<?php bloginfo('template_directory'); ?>/common/img/sidebar/side_o08.png" alt="" /></a></li>
            <li><a href="<?php echo get_permalink( get_page_by_path( 'music' ) ) ?>#sax" class="scroll01"><img src="<?php bloginfo('template_directory'); ?>/common/img/sidebar/side_o09.png" alt="" /></a></li>
            <li><a href="<?php echo get_permalink( get_page_by_path( 'music' ) ) ?>#drum" class="scroll01"><img src="<?php bloginfo('template_directory'); ?>/common/img/sidebar/side_o10.png" alt="" /></a></li>
            <li><a href="<?php echo get_permalink( get_page_by_path( 'music' ) ) ?>#voicetraining" class="scroll01"><img src="<?php bloginfo('template_directory'); ?>/common/img/sidebar/side_o11.png" alt="" /></a></li>
            <li><a href="<?php echo get_permalink( get_page_by_path( 'pilates' ) ) ?>#pilates" class="scroll01"><img src="<?php bloginfo('template_directory'); ?>/common/img/sidebar/side_o12.png" alt="" /></a></li>
          </ul>
          <ul>
            <li class="sidetit"><a href="<?php echo get_permalink( get_page_by_path( 'entertainment' ) ) ?>"><img src="<?php bloginfo('template_directory'); ?>/common/img/sidebar/side_o13.png" alt="" /></a></li>
            <li class="sidetit"><a href="<?php echo get_permalink( get_page_by_path( 'talent_entertainment' ) ) ?>"><img src="<?php bloginfo('template_directory'); ?>/common/img/sidebar/side_o14.png" alt="" /></a></li>
            <li class="sidetit"><a href="<?php echo get_permalink( get_page_by_path( 'composition' ) ) ?>"><img src="<?php bloginfo('template_directory'); ?>/common/img/sidebar/side_o15.png" alt="" /></a></li>
            <li class="sidetit"><a href="<?php echo get_permalink( get_page_by_path( 'dress' ) ) ?>"><img src="<?php bloginfo('template_directory'); ?>/common/img/sidebar/side_o16.png" alt="" /></a></li>
          </ul>
          <ul class="aside_img">
             <li><a href="http://mereliye.jp/meretere.html" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/common/img/sidebar/aside_img01.png" alt="aside_img01"></a></li>
            <li><a href=""><img src="<?php bloginfo('template_directory'); ?>/common/img/sidebar/aside_img02.png" alt="aside_img01"></a></li>
          </ul>
          <ul class="sideadd">
            <li class="sideadd01">〒544-0022 <br/>大阪市生野区舎利寺1丁目3-11</li>
            <li class="sidetel">TEL 06-6715-1986</li>
            <li class="sidetel">FAX 06-6715-3453</li>
          </ul>
        </div>
      </div><!-- sidebar -->